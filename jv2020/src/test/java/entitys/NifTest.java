package entitys;

import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class NifTest {
	
	private static Nif nifTest;
	
	@BeforeAll
	private static void initializeDefault() {
		nifTest = new Nif();
	}
	
	@Test
	void setTextTest() {
		nifTest.setText("56003575R");
		assertTrue(nifTest.getText() == "56003575R");
	}

} 
